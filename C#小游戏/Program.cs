﻿using System;



namespace Csharpe小游戏
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 控制台基础设置
            //隐藏光标
            Console.CursorVisible = false;
            //设置舞台(控制台)大小
            int w = 120;
            int h = 40;
            Console.SetWindowSize(w, h);
            Console.SetBufferSize(w, h);
            #endregion

            #region 多个场景
            //当前所在场景的编号
            int nowSceneID = 1;
            while (true)
            {
                //用标识处理想要再while循环内部的switch逻辑执行时，希望退出外层while循环
                bool isQuiteWhile = false;
                //不同场景ID 进行不同的逻辑处理
                switch (nowSceneID)
                {
                    #region 开始场景
                    //开始场景
                    case 1:
                        Console.Clear();
                        Console.SetCursorPosition(w/2-4,8 );
                        Console.Write("游戏名称");

                        //当前选项的编号
                        int nowSelIndex = 0;
                        while(true)
                        {
                            //显示内容
                            //先设置光标位置，再显示内容
                            Console.SetCursorPosition(w / 2 - 4, 13);
                            //根据当前选择的编号决定是否变色
                            Console.ForegroundColor = (nowSelIndex == 0 ? ConsoleColor.Red : ConsoleColor.White);
                            Console.Write("开始游戏");

                            Console.SetCursorPosition(w / 2 - 4, 15);
                            Console.ForegroundColor = (nowSelIndex == 1 ? ConsoleColor.Red : ConsoleColor.White);
                            Console.Write("退出游戏");

                            //检测输入
                            char intput =Console.ReadKey(true).KeyChar;
                            switch(intput)
                            {
                                case 'w':
                                case 'W':
                                    nowSelIndex--;
                                    if(nowSelIndex < 0)
                                    {
                                        nowSelIndex = 1;
                                    }
                                    break;
                                case 'S':
                                case 's':
                                    nowSelIndex++;
                                    if(nowSelIndex >1)
                                    {
                                        nowSelIndex = 0;
                                    }
                                    break;
                                case 'J':
                                case 'j':
                                    if(nowSelIndex == 0)
                                    {
                                        //改变场景ID
                                        nowSceneID = 2;
                                        //退出内层循环
                                        isQuiteWhile = true;
                                    }
                                    else
                                    {
                                        //关闭控制台
                                        Environment.Exit(0);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if(isQuiteWhile==true)
                            {
                                break;
                            }
                        }
                        break;
                    #endregion

                    //游戏场景●○★
                    case 2:
                        Console.Clear();
                        #region 地图背景
                        //设置颜色
                        Console.ForegroundColor = ConsoleColor.White;
                        //画墙                        
                        for(int i=0;i<=w-2;i+=2)
                        {
                            //画上方墙
                            Console.SetCursorPosition(i,0);
                            Console.Write("■");
                            //下方墙
                            Console.SetCursorPosition(i, h - 1);
                            Console.Write("■");
                            //中间
                            Console.SetCursorPosition(i, h-10);
                            Console.Write("■");                                                   
                        }
                        for (int i = 0; i <= h - 2; i++)
                        {
                            //左方墙
                            Console.SetCursorPosition(0, i);
                            Console.Write("■");
                            //右边
                            Console.SetCursorPosition(w - 2, i);
                            Console.Write("■");
                        }
                        #endregion

                        #region boss属性
                        int bossX = w/2;
                        int bossY = h/2;
                        int bossAtkMin = 7;
                        int bossAtkMax = 14;
                        int bossHP = 100;
                        string bossIcon = "★";
                        //申明一个颜色变量
                        ConsoleColor bossColor=ConsoleColor.Red;
                        #endregion

                        #region 玩家属性
                        int playerX = w / 10;
                        int playerY = h / 10;
                        int playerAtkMin = 8;
                        int playerAtkMax = 12;
                        int playerHP = 100;
                        string playerIcon = "●";
                        ConsoleColor playerColor = ConsoleColor.Blue;
                        //玩家输入的内容，循环外申明，节约性能
                        char playerInput;
                        #endregion

                        #region 公主属性
                        int princessX = w / 5;
                        int princessY = h / 5;
                        string princessIcon = "○";
                        ConsoleColor princessColor = ConsoleColor.Green;
                        #endregion

                        bool isFight =false;
                        bool isOver = false;
                        while (true)
                        {
                            #region boss生成
                            if (bossHP>0)
                           { 
                            //绘制boss图标
                            Console.SetCursorPosition(bossX, bossY);
                            Console.ForegroundColor = bossColor;
                            Console.Write(bossIcon);
                            }
                            #endregion

                            #region 公主
                            else
                            {
                                Console.SetCursorPosition(princessX, princessY);
                                Console.ForegroundColor=princessColor;
                                Console.Write(princessIcon);
                            }
                            #endregion


                            #region 玩家
                            //画出玩家
                            Console.SetCursorPosition(playerX,playerY);
                            Console.ForegroundColor=playerColor;
                            Console.Write(playerIcon);
                            //得到玩家输入
                            playerInput = Console.ReadKey(true).KeyChar;
                            #endregion

                            #region 战斗                           
                            if (isFight)
                            {
                                //如果正在战斗
                                if(playerInput=='j'||playerInput=='j')
                                {
                                    //判断玩家或boss是否死亡
                                    if(playerHP<=0)
                                    {
                                        //游戏结束
                                        nowSceneID = 3;
                                        break;
                                    }
                                    else if(bossHP<=0)
                                    {
                                        //擦除boss
                                        Console.SetCursorPosition(bossX, bossY);
                                        Console.Write("  ");
                                        isFight = false;                                        
                                    }
                                    else
                                    {
                                        //玩家打boss
                                        Random r = new Random();
                                        //得到随机攻击力
                                        int atk = r.Next(playerAtkMin, playerAtkMax);
                                        //boss血量减少
                                        bossHP -= atk;
                                        //打印状态
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        //先擦除上一次显示的内容
                                        Console.SetCursorPosition(2, h - 4);
                                        Console.Write("                                                        ");
                                        Console.SetCursorPosition(2, h - 4);
                                        Console.Write("你对boss造成{0}伤害，boss剩余血量{1}", atk, bossHP);
                                        if (bossHP > 0)
                                        {
                                            atk = r.Next(bossAtkMin, bossAtkMax);
                                            playerHP -= atk;

                                            //打印状态
                                            Console.ForegroundColor = ConsoleColor.Blue;
                                            //先擦除上一次显示的内容
                                            Console.SetCursorPosition(2, h - 3);
                                            Console.Write("                                                        ");
                                            Console.SetCursorPosition(2, h - 3);
                                            Console.Write("boss对你造成{0}伤害，你剩余血量{1}", atk, playerHP);
                                            if (playerHP <= 0)
                                            {
                                                //显示失败信息
                                                Console.SetCursorPosition(2, h - 2);
                                                Console.Write("你已死,游戏失败,按J继续");
                                            }
                                        }
                                        else
                                        {
                                            //擦除之前战斗信息
                                            Console.SetCursorPosition(2, h - 5);
                                            Console.Write("                                                        ");
                                            Console.SetCursorPosition(2, h - 4);
                                            Console.Write("                                                        ");
                                            Console.SetCursorPosition(2, h - 3);
                                            Console.Write("                                                        ");
                                            //显示胜利信息
                                            Console.SetCursorPosition(2, h - 5);
                                            Console.Write("boss已死,按J继续");
                                        }
                                    }                                    
                                }
                            }
                            else
                            {
                                #region 玩家移动
                                //擦除
                                Console.SetCursorPosition(playerX, playerY);
                                Console.Write("  ");
                                //改位置
                                switch (playerInput)
                                {
                                    case 'w':
                                    case 'W':
                                        playerY--;
                                        if (playerY < 1)
                                        {
                                            playerY = 1;
                                        }
                                        //位置和boss重合且boss未死
                                        else if (playerX == bossX && playerY == bossY && bossHP != 0)
                                        {
                                            playerY++;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHP <= 0)
                                        {
                                            playerY ++;
                                        }
                                        break;
                                    case 'S':
                                    case 's':
                                        playerY++;
                                        if (playerY > h - 11)
                                        {
                                            playerY = h - 11;
                                        }
                                        //位置和boss重合且boss未死
                                        else if (playerX == bossX && playerY == bossY && bossHP != 0)
                                        {
                                            playerY--;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHP <= 0)
                                        {
                                            playerY --;
                                        }
                                        break;
                                    case 'A':
                                    case 'a':
                                        playerX -= 2;
                                        if (playerX < 2)
                                        {
                                            playerX = 2;
                                        }
                                        //位置和boss重合且boss未死
                                        else if (playerX == bossX && playerY == bossY && bossHP != 0)
                                        {
                                            playerX += 2;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHP <= 0)
                                        {
                                            playerX += 2;
                                        }
                                        break;
                                    case 'D':
                                    case 'd':
                                        playerX += 2;
                                        if (playerX > w - 4)
                                        {
                                            playerX = w - 4;
                                        }
                                        //位置和boss重合且boss未死
                                        else if (playerX == bossX && playerY == bossY && bossHP != 0)
                                        {
                                            playerX -= 2;
                                        }
                                        else if (playerX == princessX && playerY==princessY&&bossHP<=0)
                                        {
                                            playerX -= 2;
                                        }
                                        break;
                                    case 'J':
                                    case 'j':
                                        //开始战斗
                                        if ((playerX == bossX && playerY == bossY - 1 ||
                                            playerX == bossX && playerY == bossY + 1 ||
                                            playerX == bossX - 2 && playerY == bossY ||
                                            playerX == bossX + 2 && playerY == bossY) && bossHP > 0)
                                        {
                                            isFight = true;
                                            Console.SetCursorPosition(2, h - 5);
                                            Console.ForegroundColor = ConsoleColor.White;
                                            Console.Write("开始战斗，按J继续");
                                            Console.SetCursorPosition(2, h - 4);
                                            Console.Write("玩家当前血量{0}", playerHP);
                                            Console.SetCursorPosition(2, h - 3);
                                            Console.Write("boss当前血量{0}", bossHP);
                                        }
                                        //判断是否在公主身边按J
                                        else if((playerX == princessX && playerY == princessY - 1 ||
                                            playerX == princessX && playerY == princessY + 1 ||
                                            playerX == princessX - 2 && playerY == princessY ||
                                            playerX == princessX + 2 && playerY == princessY) && bossHP <= 0)
                                        {
                                            //改变场景ID,跳出游戏界面的while循环
                                            nowSceneID = 3;
                                            isOver=true;
                                            break;
                                        }
                                        break;
                                        #endregion
                                }
                            }
                            #endregion    
                            if(isOver)
                            {
                                break;
                            }
                        }
                        break;
                    #region
                    //结束场景
                    case 3:
                        Console.Clear();

                        Console.SetCursorPosition(w / 2 - 4, 8);
                        Console.Write("GAMEOVER");

                        Console.SetCursorPosition(w / 2 - 8, 8);
                        Console.Write("GAMEOVERINFO");

                        //当前选项的编号
                        int nowSelEndIndex = 0;
                        while (true)
                        {
                            bool isQuiteEndWhile = false;

                            //显示内容
                            //先设置光标位置，再显示内容
                            Console.SetCursorPosition(w / 2 - 4, 13);
                            //根据当前选择的编号决定是否变色
                            Console.ForegroundColor = (nowSelEndIndex == 0 ? ConsoleColor.Red : ConsoleColor.White);
                            Console.Write("开始游戏");

                            Console.SetCursorPosition(w / 2 - 4, 15);
                            Console.ForegroundColor = (nowSelEndIndex == 1 ? ConsoleColor.Red : ConsoleColor.White);
                            Console.Write("退出游戏");

                            //检测输入
                            char intput = Console.ReadKey(true).KeyChar;
                            switch (intput)
                            {
                                case 'w':
                                case 'W':
                                    nowSelEndIndex--;
                                    if (nowSelEndIndex < 0)
                                    {
                                        nowSelEndIndex = 1;
                                    }
                                    break;
                                case 'S':
                                case 's':
                                    nowSelEndIndex++;
                                    if (nowSelEndIndex > 1)
                                    {
                                        nowSelEndIndex = 0;
                                    }
                                    break;
                                case 'J':
                                case 'j':
                                    if (nowSelEndIndex == 0)
                                    {
                                        //改变场景ID
                                        nowSceneID = 1;
                                        isQuiteEndWhile=true;
                                    }
                                    else
                                    {
                                        //关闭控制台
                                        Environment.Exit(0);
                                    }
                                    break;
                            }
                            if (isQuiteEndWhile == true)
                            {
                                break;
                            }
                        }
                        break;
                        #endregion

                        break;
                }
            }
            #endregion
        }
    }
}
