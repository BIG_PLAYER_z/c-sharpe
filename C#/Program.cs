﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

//命名空间 代码块

//class Program
//{
//    //类 代码块
//    //面向对象相关
//    static void Main(string[] args)
//    {
        #region MyRegion


//函数 代码块

//在控制台打印一行信息，打印信息结束后会自动空一行
//Console.WriteLine("Hello, World!");

////在控制台打印信息，但不自动空行
//Console.Write("Hello ");
//Console.Write("World");

//输入
//Console.ReadLine();

//检测玩家是否按键，只要按了键盘上的任意键，就会认为输入结束
//Console.ReadKey();
//char c=Console.ReadKey(true).KeyChar;//()里加true就不会把输入的内容打印在控制台上

#endregion
//    }
//}


//class Program
//{
//    static void Main(string[] args)
//    {
        //折叠代码 - 使编程时逻辑更加清晰，可以将中间包含的代码折叠起来，避免代码过于凌乱
        //本质是编辑器提供的预处理指令，只会在编辑是有用，执行代码时会被自动删除
        #region 申明变量

        //申明变量：变量类型 变量名 = 初始值；
        //int i = 0;

        //变量类型共14种
        //1.有符号的整型变量：能存储一定范围正负数包括0的变量类型：
        //  sbyte     -128`127
        //  int       -21亿~21亿
        //  short     -32768~32767
        //  long      -九百万兆~九百万兆

        //2.无符号的整型变量：能存储一定范围0和正数的变量类型
        //  byte    0~255
        //  uint    0~42亿
        //  ushort  0~65535
        //  ulong   0~18百万兆

        //3.浮点型:
        //  float   存储7~8位有效数字，根据编译器不同，有效数字可能也不同(有效数字指从第一个非0数开始),申明变量时数字结尾加f
        //  double  存储15~17位有效数字 - 小数默认为double类型
        //  decimal 存储27~28位的有效数字 - 数字结尾加m

        //4.特殊类型：
        //  bool    true/false - 表示真假的数据类型
        //  char    存储单个字符的变量类型
        //  string  存储字符串的变量类型


        #endregion

        #region @ - 取消转义字符
        //@ - 取消转义字符
        #endregion

        #region 类型转换 - 隐式转换
        //基本规则：1.不同类型之间的自动转换；2.大范围装小范围

        //float可以隐式转换为double - decimal不能用隐式转换的方式去存储double和float

        //不同大类型之间的转换
        //无符号可以转换为有符号（前提是范围涵盖），但有符号类型不能转换为无符号
        //整型可以隐式转换为浮点型，decimal不能存储double和floa，但可以存储整型；但整型不能存储浮点型
        //bool型无法和其他类型进行隐式转换，char无法存储其他类型，但可以转换为整型（对应的数值时ASCLL码），string无法和其他类型进行隐式转换

        //高精度（大范围）装低精度（小范围）

        #endregion

        #region 类型转换 - 显式转换 - 括号强转
        //手动处理 - 强制转换
        //括号强转
        //变量类型 变量名 = (变量类型)变量;
        //注意：精度问题 范围问题

        //浮点型强转为整型时，会直接抛弃小数点后的数值
        #endregion

        #region 显式转换 - Parse法
        //作用：把字符串类型转换为对应的类型
        //语法：变量类型 变量名 = 变量类型.Parse("字符串");
        //注意：字符串必须能够转换为对应类型，否则报错；值的范围必须是能够被变量存储的值，否则报错
        #endregion

        #region 显式转换 - Convert法
        //作用：更准确的将各个类型之间进行相互转换
        //语法：Convert.To目标类型(变量或常量)
        //注意：填写的变量或常量必须正确，否则报错

        //int a = Convert.ToInt32("123");
        //Console.WriteLine(a);
        ////精度比括号强转精确，会四舍五入
        //a = Convert.ToInt32(3.1415926);

        //short b = Convert.ToInt16(a);
        //Console.WriteLine(b);
        #endregion

        #region 显式转换 - 其他类型转string
        //作用：拼接打印
        //语法：变量.ToString();
        //int a=0; 
        //string str1=1.ToString();
        //string str2=a.ToString();
        ////进行字符串拼接时，会自动调用 ToString 转换为string
        //Console.WriteLine(str1+"abcdef"+a);
        #endregion

        #region 异常捕获
        //作用：避免当代码报错时，程序卡死
        //语法：
        //必备部分
        //try
        //{
        //    //希望进行异常捕获的代码块
        //    //如果try中的代码报错了，不会让程序卡死
        //}
        //catch(Exception e)
        //{
        //    //如果程序出错，会执行catch代码，来捕获异常
        //    //(Exception e)具体的报错跟踪，通过e得到具体的错误信息
        //}
        ////可选部分
        //finally
        //{
        //    //最后执行的代码，不管有没有出错，都会执行其中的代码
        //}


        //try
        //{
        //    string str=Console.ReadLine();
        //    int i=int.Parse(str);
        //    Console.WriteLine(i);
        //}
        //catch
        //{
        //    Console.WriteLine("请输入合法数字");
        //}
        //finally
        //{
        //    Console.WriteLine("执行完毕");
        //}
        #endregion

        #region 字符串拼接
        //用+进行字符串拼接
        //string str = "123";
        //str +="456";
        //Console.WriteLine(str);
        //str = str + 1;//自动调用 1.ToString();
        //str += "123" + 4 + true;
        //str += 1 + 2 + 3 + 4;//先进行右侧的计算，再进行赋值
        //str += " " + 1 + 2 + 3 + 4;//右侧计算类型取决于第一个进行计算的数
        //str += 1 + 2 + " " + 3 + 4;//打印3 3 4
        //Console.WriteLine(str);

        //固定语法
        //string.Formmat("待拼接的内容",内容1,内容2,……);
        //想被拼接的内容用占位符替代{数字} 数字0~n
        //string str=string.Format("我是{0}，今年{1}岁，{2},{3}", "WZZ", 18, "hhhh",true);
        //Console.WriteLine(str);

        //控制台打印拼接
        //Console.WriteLine("我是{0}，今年{1}岁，{2},{3}", "WZZ", 18, "hhhh", true);
        //后面的内容比占位符多，不会报错
        //后面的内容比占位符少，会报错
        #endregion

        #region 控制台
        //  清空  
        //Console.Clear();

        //设置控制台大小
        //1.先设置窗口大小，再设置缓冲区大小；2.缓冲区大小不能小于窗口大小；3.窗口的大小不能大于控制台的最大尺寸
        //窗口大小
        //Console.SetWindowSize(100, 50);
        ////缓冲区大小（可打印内容区域的宽高）
        //Console.SetBufferSize(1000, 1000);

        ////设置光标位置
        //Console.SetCursorPosition(10, 5);
        //Console.WriteLine("77777");

        ////设置颜色
        ////文字颜色设置
        //Console.ForegroundColor = ConsoleColor.Green;
        //Console.WriteLine("7777777");
        //Console.ForegroundColor= ConsoleColor.Black;   
        ////背景颜色
        //Console.BackgroundColor = ConsoleColor.Blue;
        //Console.Clear();
        //Console.WriteLine("456123789");

        ////光标显隐
        //Console.CursorVisible = false;

        ////关闭控制台
        //Environment.Exit(0);

        #endregion

        #region 随机数
        //产生随机数对象
        //Random rnd = new Random();

        ////生成随机数
        //int i=rnd.Next();//生成一个非负数的随机数
        //Console.WriteLine(i);

        //i=rnd.Next(100);//生成一个0~99的随机数
        //Console.WriteLine(i);

        //i= rnd.Next(7,77);//生成一个7~76的随机数(左闭右开区间）
        //Console.WriteLine(i);
        #endregion

        #region 数组
        //数组的声明
        //int[] arr;
        //int[] arr1 = new int[5];
        //int[] arr2 = new int[5] { 1, 2, 3, 4, 5, };
        //int[] arr3= new int[] { 1, 2, 3 };
        //int[] arr4 = { };

        //数组中的长度 - 数组名.Length
        //int[] arr = { 1, 2, 3, 4, 5};
        //Console.WriteLine(arr.Length);


        //二维数组
        //int[,] arr;
        //int[,] arr2 = new int[3, 3];
        //int[,] arr3 = new int[3, 3] { { 1, 1, 1 }, { 2, 2, 2, }, { 3, 3, 3 } };
        //int[,] arr4 = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        //int[,] arr5 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; 

        //二维数组的长度
        //int[,] arr = { { 1, 2, 3 }, { 4, 5, 6 } };
        ////得到多少行
        //Console.WriteLine(arr.GetLength(0));
        ////得到多少列
        //Console.WriteLine(arr.GetLength(1));


        //交错数组 - 数组的数组，每个维度的数量可不同 - 二维数组每行的列数相同，交错数组每行的列数可能不同
        //int[][] arr1;
        //int[][]arr2=new int[3][];
        //int[][]arr3=new int[3][] { new int[] { 1, 2, 3 }, new int[] { 4, 5 },new int[] { 7 } };
        //int[][] arr4 = new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5 }, new int[] { 7 } };
        //int[][] arr5 = { new int[] { 1, 2, 3 }, new int[] { 4, 5 }, new int[] { 6 } };

        //交错数组的长度
        //int[][] arr = { new int[] { 1, 2, 3 }, new int[] { 4, 5 }, new int[] { 6 } };
        ////行
        //Console.WriteLine(arr.GetLength(0));
        ////列
        //Console.WriteLine(arr[0].Length);//得到某一行的长度

        ////遍历
        //for(int i=0;i<arr.GetLength(0);i++)
        //{
        //    for(int j = 0; j < arr[i].Length;j++)
        //    {
        //        Console.Write(arr[i][j] + " ");
        //    }
        //    Console.WriteLine();
        //}
        #endregion

        #region 使用和存储上的区别
        //int a = 10;
        //int[] arr = new int[] { 1, 2, 3, 4, 5 };

        //int b = a;
        //int[] arr2 = arr;
        //Console.WriteLine("a=={0},b=={1}", a, b);
        //Console.WriteLine("arr[0]=={0},arr2[0]=={1}", arr[0], arr2[0]);

        //b = 20;
        //arr2[0] = 7;
        //Console.WriteLine("修改之后");
        //Console.WriteLine("a=={0},b=={1}", a, b);
        //Console.WriteLine("arr[0]=={0},arr2[0]=={1}", arr[0], arr2[0]);

        ////值类型在相互赋值时，是把内容拷贝，被拷贝的变，被赋值的不变 - 引用类型的相互赋值时，是让两者指向同一个值，二者同时变化

        ////为什么有以上区别
        ////值类型和引用类型存储在内存区域是不同的，存储方式是不同的
        ////值类型存储在栈空间 - 系统分配，自动回收，小而快
        ////引用类型存储在堆空间 - 手动申请和释放，大而慢

        //arr2 = new int[]{10,1,2,3,4};//开辟了新的空间来存储新的数组
        //Console.WriteLine(arr2[0]);
        //Console.WriteLine("arr[0]=={0},arr2[0]=={1}", arr[0], arr2[0]);
        #endregion

        #region string
        //值类型 - 存储在栈内存中
        //无符号整型、有符号整型、浮点型、char、bool、结构体

        //引用类型 - 存储在堆内存中
        //数组、string、类

        //string str1 = "123";
        //string str2 = str1;
        //str2 = "456";
        //Console.WriteLine(str1);
        //Console.WriteLine(str2);
        //string类型在重新赋值时，会在堆中重新分配空间 - 频繁的改变string的值会产生内存垃圾，造成空间浪费
        #endregion

//    }
//}


//class Program
//{
    #region 函数使用
    //函数语法
    //static 返回类型 函数名(参数类型 参数名1，参数类型 参数名2……)
    //static int Add(int x,int y)
    //{
    //    return x + y;
    //}
    //static int[] Add(int x, int y)
    //{
    //    int sum = x + y;
    //    int avg = sum / 2;
    //    //int[]arr={sum,avg};
    //    //return arr;
    //    return new int[] { sum, avg };
    //    //如果用数组作为返回值，前提是使用者知道这个数组的规则
    //}
    #endregion

    #region ref和out相关函数
    //static void ChangeValue(int value)
    //{
    //    value = 3;

    //}
    //static void ChangeArrValue(int[]arr)
    //{
    //    arr[0] = 99;

    //}
    //static void ChangeArr(int[] arr)
    //{

    //    arr = new int[] { 10, 20, 30 };
    //}


    ////ref
    //static void ChangeValue(ref int value)
    //{
    //    value = 7;
    //}

    //static void ChangeArrValue(ref int[] arr)
    //{
    //    arr = new int[] { 10, 20, 30 };
    //}


    ////out
    //static void ChangeValue(out int value)
    //{
    //    value = 7;
    //}

    //static void ChangeArrValue(out int[] arr)
    //{
    //    arr = new int[] { 10, 20, 30 };
    //}


    //ref 和 out 区别
    //1.ref传入的变量必须初始化,out不用
    //2.out传入的变量必须在内部赋值，ref不用

    //ref传入的变量必须初始化，但是在函数内部可改可不改
    //out传入的变量不用初始化，但是在内部必须赋值(修改该值)

    #endregion

    #region 变长参数和参数默认值
    //params - 变长函数
    //params int[]意味着可以传入n个int参数，n可以等于零，传入的参数会存在arr数组中
    //注意：1.params关键字后面必须是数组；2.数组类型可以为任意类型
    //      3.函数参数可以有别的参数和params关键字修饰的参数；
    //      4.函数参数中最多只能出现一个params关键字，并且一定是在最后一组参数，前面可以有n个其他参数
    //static int Sum(params int[] arr)
    //{
    //    int sum = 0;
    //    for(int i=0;i<arr.Length;i++)
    //    {
    //        sum+= arr[i];
    //    }
    //    return sum;
    //}

    //函数默认值
    //有参数默认值的参数一般称为可选参数
    //作用：当调用函数时可以不传入参数，不传就会使用默认值作为参数的值
    //注意：1.支持多参数默认值，每个参数都可以有默认值
    //      2.如果要混用可选参数，必须写在普通参数后面
    //static void Speak(string str="66666")
    //{
    //    Console.WriteLine(str);
    //}

    #endregion

    #region 函数重载
    //重载 - 在同一语句块(class和struct)中，函数名相同，参数的数量不同，或者，参数的数量相同但参数的类型和顺序不同
    //作用：1.命名一组功能相似的函数，减少函数名的数量，避免命名空间的污染；2.提升程序可读性
    //注意：重载和返回值类型无关，只和参数类型、个数、顺序有关；2.调用时，程序会自己根据传入的参数类型判断使用哪一个重载
    //static int Sum(int a,int b)
    //{
    //    return a + b;
    //}
    ////参数数量不同
    //static int Sum(int a,int b,int c)
    //{
    //    return a + b + c;
    //}
    ////数量相同，类型不同
    //static float Sum(float a,float b)
    //{
    //    return a + b;
    //}
    ////数量相同，类型不同
    //static float Sum(int a,float b) 
    //{  
    //    return a + b; 
    //}
    ////数量相同，顺序不同
    //static float Sum(float a,int b)
    //{
    //    return (float)a + b;
    //}
    //ref和out可以理解为一种变量类型，所以可以用在重载中，但ref和out不能同时修饰；默认变量不能用于重载
    #endregion

    #region 结构体
    ////结构体 - 自定义变量类型
    //struct Stu
    //{
    //    //变量
    //    //private - 私有的 - 默认不写私有，即仅能在内部使用
    //    public int age;
    //    public bool sex;
    //    public int num;//public - 公开的 - 可以在外部使用
    //    public string name;
    //    //构造函数（可选）
    //    //1.结构体的函数没有返回值；2.函数名必须和结构体名相同；3.必须有参数；4.如果申明了构造函数，必须对其中所有变量数据初始化
    //    //一般用于方便在外初始化
    //    public Stu(int age, bool sex, int num, string name)
    //    {
    //        //this - 代表自己
    //        this.age = age;
    //        this.sex = sex;
    //        this.num = num;
    //        this.name = name;
    //    }


    //    //函数 - 表现数据结构的行为
    //    //在结构体的函数不需要static关键字
    //    public void speak()
    //    {
    //        //函数中可以直接使用结构体内申明的变量
    //        Console.WriteLine("名字叫{0}，年龄{1}，", name, age);
    //    }
    //}
    #endregion

    #region 冒泡排序
    //static void Bubble(int[]arr)
    //{
    //    for (int i = 0; i < arr.Length; i++)
    //    {
    //        for (int j = 0; j < arr.Length - 1 - i; j++)
    //        {
    //            bool flag = false;
    //            if (arr[j] > arr[j + 1])
    //            {
    //                int temp = arr[j];
    //                arr[j] = arr[j + 1];
    //                arr[j + 1] = temp;
    //                flag = true;
    //            }
    //            if(flag==false)
    //            {
    //                return;
    //            }
    //        }
    //    }
    //}

    //static void Print(int[]arr)
    //{
    //    for (int i = 0; i < arr.Length; i++)
    //    {
    //        Console.Write(arr[i]);
    //        Console.Write(" ");
    //    }
    //}
    #endregion

    #region 选择排序
    //static void Print(int[]arr)
    //{
    //    for (int i = 0; i < arr.Length; i++)
    //    {
    //        Console.Write(arr[i]+" ");

    //    }
    //}
    #endregion

    //static void Main(string[] args)
    //{
        #region 函数使用
        //int[] arr = Add(5, 7);
        //Console.WriteLine(arr[0]+" " + arr[1]);
        #endregion

        #region ref和out
        //ref 和 out - 在函数内部改变外部传入的内容

        //int a = 1;
        //ChangeValue(a);
        //ChangeValue(ref a);
        //ChangeValue(out a);
        //Console.WriteLine(a);

        //int[] arr = { 1, 2, 3 };
        //ChangeArrValue(arr);
        //ChangeArrValue(ref arr);
        //ChangeArrValue(out arr);
        //Console.WriteLine(arr[0]);

        //ChangeArr(arr);
        //Console.WriteLine(arr[0]);
        #endregion

        #region 变长参数和参数默认值

        //int sum=Sum(1, 2, 3, 4, 5, 67, 8, 9, 11, 12, 13, 14, 15, 16);
        //Console.WriteLine(sum);
        //Speak();
        #endregion

        #region 函数重载
        //Console.WriteLine("HE");
        //Sum(1, 2);
        //Sum(1, 2, 3);
        //Sum(2.1f, 3.14f);
        //Sum(2.1f, 3.14f);
        //Sum(2, 3.14f);
        //Sum(3.14f,2);
        #endregion

        #region 结构体
        //    Stu s1;
        //s1.age = 18;
        //s1.sex = false;
        //s1.num = 4346;
        //s1.name = "WZZ";
        //s1.speak();
        //Stu s2=new Stu(18,true,123456,"ahahah");
        //s2.speak();

        #endregion

        #region 冒泡排序
        //int[] arr = new int[10]{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        //Bubble(arr);
        //Print(arr);

        #endregion

        #region 选择排序
        //int[]arr=new int[10] {9,8,7,6,5,4,3,2,1,0};
        //for(int i=0; i < arr.Length; i++)
        //{
        //    int index = 0;
        //    for (int j=1;j<arr.Length-i;j++)
        //    {
        //        if (arr[index] < arr[j])
        //        {
        //            index = j;
        //        }
        //    }
        //    if(index!=arr.Length-1-i)
        //    {
        //        int temp = arr[index];
        //        arr[index] = arr[arr.Length-1-i];
        //        arr[arr.Length-1-i] = temp;
        //    }
        //}
        //Print(arr);
        #endregion

//    }
//}


class Program
{
    #region 

    #endregion

    static void Main(string[] args)
    {
        #region 

        #endregion


    }
}